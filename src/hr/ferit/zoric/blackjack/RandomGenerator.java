package hr.ferit.zoric.blackjack;

import java.util.Random;

public class RandomGenerator {
	private static Random instance;
	
	private RandomGenerator(){
		instance = new Random();
	}
	
	public static Random getInstance(){
		return instance;
	}
}
