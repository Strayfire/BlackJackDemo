package hr.ferit.zoric.blackjack;

import java.util.Scanner;

public class ConsoleUserInput implements IUserInput {

	@Override
	public boolean getUserSelection() {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		boolean shouldHit = input.equals("y") ? true : false;
		return shouldHit;
	}

}
