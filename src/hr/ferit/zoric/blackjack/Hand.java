package hr.ferit.zoric.blackjack;

import java.util.ArrayList;

public class Hand {

	private ArrayList<Card> mCards;
	
	public Hand(){
		this.mCards = new ArrayList<>();
	}
	
	public void addDrawnCard(Card card){
		this.mCards.add(card);
	}
	
	public int getHandValue(){
		
		int sum = 0;		
		int aceCount=0;
		
		for(Card card : this.mCards){			
			sum += this.valueOf(card);
			if(card.getFace() == Face.ACE) {
				aceCount++;
			}
		}
		
		while(sum > 21 && aceCount > 0) {
			sum -= 10;
			aceCount--;
		}
		return sum;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Card card : this.mCards){
			builder.append(card).append(",");
		}
		builder.append(" [").append(this.getHandValue()).append("]");
		return builder.toString();
	}

	private int valueOf(Card card) {
		switch(card.getFace()){
		case ACE: 	return 11;		
		case TWO: 	return 2;
		case THREE: return 3;
		case FOUR: 	return 4;
		case FIVE: 	return 5;
		case SIX: 	return 6;
		case SEVEN: return 7;
		case EIGHT: return 8;
		case NINE: 	return 9;
		case TEN: 	return 10;
		case JACK: 	return 10;
		case QUEEN: return 10;
		case KING: 	return 10;
		default: 	return 0;
		}		
	}

	public int getHandCount() {
		return this.mCards.size();
	}
}
