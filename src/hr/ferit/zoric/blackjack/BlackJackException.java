package hr.ferit.zoric.blackjack;

public class BlackJackException extends RuntimeException{
	
	public BlackJackException(String message){
		super(message);
	}
	
	public static class DeckException extends BlackJackException{
		public DeckException(String message){
			super(message);
		}
	}

}
