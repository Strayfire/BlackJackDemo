package hr.ferit.zoric.blackjack;

public class Card {
	private Face mFace;	
	private Suite mSuite;
	
	public Card(Face face, Suite suite){
		this.mFace = face;
		this.mSuite = suite;
	}

	public Face getFace() {
		return mFace;
	}

	public Suite getSuite() {
		return mSuite;
	}
	
	@Override
	public String toString() {
		return this.mFace + " of " + this.mSuite;
	}
}

