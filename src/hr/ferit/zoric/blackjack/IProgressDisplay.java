package hr.ferit.zoric.blackjack;

public interface IProgressDisplay {
	public void displayDealerWin();
	public void displayPlayerWin();
	public void displayDraw();
	public void displayHitMePrompt();
	public void displayHand(Hand hand);
}
